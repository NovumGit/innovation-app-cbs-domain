<?php

namespace Model\Custom\NovumCbs\DataGeneratie;

use Model\Custom\NovumCbs\DataGeneratie\Base\DelegatedTasks as BaseDelegatedTasks;

/**
 * Skeleton subclass for representing a row from the 'delegated_tasks' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DelegatedTasks extends BaseDelegatedTasks
{

}

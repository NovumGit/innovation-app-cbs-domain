<?php

namespace Model\Custom\NovumCbs\DataGeneratie;

use Model\Custom\NovumCbs\DataGeneratie\Base\CurrenttimeQuery as BaseCurrenttimeQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'current_time' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CurrenttimeQuery extends BaseCurrenttimeQuery
{

}

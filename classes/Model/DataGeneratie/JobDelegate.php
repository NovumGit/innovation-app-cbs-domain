<?php

namespace Model\Custom\NovumCbs\DataGeneratie;

use Model\Custom\NovumCbs\DataGeneratie\Base\JobDelegate as BaseJobDelegate;

/**
 * Skeleton subclass for representing a row from the 'job_delegate' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class JobDelegate extends BaseJobDelegate
{

}

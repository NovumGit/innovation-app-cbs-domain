<?php

namespace Model\Custom\NovumCbs\Data\Map;

use Model\Custom\NovumCbs\Data\InitialPeople;
use Model\Custom\NovumCbs\Data\InitialPeopleQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'data_initial' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class InitialPeopleTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Custom.NovumCbs.Data.Map.InitialPeopleTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'data_initial';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Custom\\NovumCbs\\Data\\InitialPeople';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Custom.NovumCbs.Data.InitialPeople';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'data_initial.id';

    /**
     * the column name for the jaartal field
     */
    const COL_JAARTAL = 'data_initial.jaartal';

    /**
     * the column name for the mannen field
     */
    const COL_MANNEN = 'data_initial.mannen';

    /**
     * the column name for the vrouwen field
     */
    const COL_VROUWEN = 'data_initial.vrouwen';

    /**
     * the column name for the leeftijd_0_20 field
     */
    const COL_LEEFTIJD_0_20 = 'data_initial.leeftijd_0_20';

    /**
     * the column name for the leeftijd_20_45 field
     */
    const COL_LEEFTIJD_20_45 = 'data_initial.leeftijd_20_45';

    /**
     * the column name for the leeftijd_45_65 field
     */
    const COL_LEEFTIJD_45_65 = 'data_initial.leeftijd_45_65';

    /**
     * the column name for the leeftijd_65_80 field
     */
    const COL_LEEFTIJD_65_80 = 'data_initial.leeftijd_65_80';

    /**
     * the column name for the leeftijd_80_106 field
     */
    const COL_LEEFTIJD_80_106 = 'data_initial.leeftijd_80_106';

    /**
     * the column name for the landsdeel_noord field
     */
    const COL_LANDSDEEL_NOORD = 'data_initial.landsdeel_noord';

    /**
     * the column name for the landsdeel_oost field
     */
    const COL_LANDSDEEL_OOST = 'data_initial.landsdeel_oost';

    /**
     * the column name for the landsdeel_west field
     */
    const COL_LANDSDEEL_WEST = 'data_initial.landsdeel_west';

    /**
     * the column name for the landsdeel_zuid field
     */
    const COL_LANDSDEEL_ZUID = 'data_initial.landsdeel_zuid';

    /**
     * the column name for the burgerlijke_ongehuwd field
     */
    const COL_BURGERLIJKE_ONGEHUWD = 'data_initial.burgerlijke_ongehuwd';

    /**
     * the column name for the burgerlijke_gehuwd field
     */
    const COL_BURGERLIJKE_GEHUWD = 'data_initial.burgerlijke_gehuwd';

    /**
     * the column name for the burgerlijke_gescheiden field
     */
    const COL_BURGERLIJKE_GESCHEIDEN = 'data_initial.burgerlijke_gescheiden';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Jaartal', 'Mannen', 'Vrouwen', 'Leeftijd020', 'Leeftijd2045', 'Leeftijd4565', 'Leeftijd6580', 'Leeftijd80106', 'LandsdeelNoord', 'LandsdeelOost', 'LandsdeelWest', 'LandsdeelZuid', 'BurgerlijkeOngehuwd', 'BurgerlijkeGehuwd', 'BurgerlijkeGescheiden', ),
        self::TYPE_CAMELNAME     => array('id', 'jaartal', 'mannen', 'vrouwen', 'leeftijd020', 'leeftijd2045', 'leeftijd4565', 'leeftijd6580', 'leeftijd80106', 'landsdeelNoord', 'landsdeelOost', 'landsdeelWest', 'landsdeelZuid', 'burgerlijkeOngehuwd', 'burgerlijkeGehuwd', 'burgerlijkeGescheiden', ),
        self::TYPE_COLNAME       => array(InitialPeopleTableMap::COL_ID, InitialPeopleTableMap::COL_JAARTAL, InitialPeopleTableMap::COL_MANNEN, InitialPeopleTableMap::COL_VROUWEN, InitialPeopleTableMap::COL_LEEFTIJD_0_20, InitialPeopleTableMap::COL_LEEFTIJD_20_45, InitialPeopleTableMap::COL_LEEFTIJD_45_65, InitialPeopleTableMap::COL_LEEFTIJD_65_80, InitialPeopleTableMap::COL_LEEFTIJD_80_106, InitialPeopleTableMap::COL_LANDSDEEL_NOORD, InitialPeopleTableMap::COL_LANDSDEEL_OOST, InitialPeopleTableMap::COL_LANDSDEEL_WEST, InitialPeopleTableMap::COL_LANDSDEEL_ZUID, InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD, InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD, InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN, ),
        self::TYPE_FIELDNAME     => array('id', 'jaartal', 'mannen', 'vrouwen', 'leeftijd_0_20', 'leeftijd_20_45', 'leeftijd_45_65', 'leeftijd_65_80', 'leeftijd_80_106', 'landsdeel_noord', 'landsdeel_oost', 'landsdeel_west', 'landsdeel_zuid', 'burgerlijke_ongehuwd', 'burgerlijke_gehuwd', 'burgerlijke_gescheiden', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Jaartal' => 1, 'Mannen' => 2, 'Vrouwen' => 3, 'Leeftijd020' => 4, 'Leeftijd2045' => 5, 'Leeftijd4565' => 6, 'Leeftijd6580' => 7, 'Leeftijd80106' => 8, 'LandsdeelNoord' => 9, 'LandsdeelOost' => 10, 'LandsdeelWest' => 11, 'LandsdeelZuid' => 12, 'BurgerlijkeOngehuwd' => 13, 'BurgerlijkeGehuwd' => 14, 'BurgerlijkeGescheiden' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'jaartal' => 1, 'mannen' => 2, 'vrouwen' => 3, 'leeftijd020' => 4, 'leeftijd2045' => 5, 'leeftijd4565' => 6, 'leeftijd6580' => 7, 'leeftijd80106' => 8, 'landsdeelNoord' => 9, 'landsdeelOost' => 10, 'landsdeelWest' => 11, 'landsdeelZuid' => 12, 'burgerlijkeOngehuwd' => 13, 'burgerlijkeGehuwd' => 14, 'burgerlijkeGescheiden' => 15, ),
        self::TYPE_COLNAME       => array(InitialPeopleTableMap::COL_ID => 0, InitialPeopleTableMap::COL_JAARTAL => 1, InitialPeopleTableMap::COL_MANNEN => 2, InitialPeopleTableMap::COL_VROUWEN => 3, InitialPeopleTableMap::COL_LEEFTIJD_0_20 => 4, InitialPeopleTableMap::COL_LEEFTIJD_20_45 => 5, InitialPeopleTableMap::COL_LEEFTIJD_45_65 => 6, InitialPeopleTableMap::COL_LEEFTIJD_65_80 => 7, InitialPeopleTableMap::COL_LEEFTIJD_80_106 => 8, InitialPeopleTableMap::COL_LANDSDEEL_NOORD => 9, InitialPeopleTableMap::COL_LANDSDEEL_OOST => 10, InitialPeopleTableMap::COL_LANDSDEEL_WEST => 11, InitialPeopleTableMap::COL_LANDSDEEL_ZUID => 12, InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD => 13, InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD => 14, InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'jaartal' => 1, 'mannen' => 2, 'vrouwen' => 3, 'leeftijd_0_20' => 4, 'leeftijd_20_45' => 5, 'leeftijd_45_65' => 6, 'leeftijd_65_80' => 7, 'leeftijd_80_106' => 8, 'landsdeel_noord' => 9, 'landsdeel_oost' => 10, 'landsdeel_west' => 11, 'landsdeel_zuid' => 12, 'burgerlijke_ongehuwd' => 13, 'burgerlijke_gehuwd' => 14, 'burgerlijke_gescheiden' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('data_initial');
        $this->setPhpName('InitialPeople');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Custom\\NovumCbs\\Data\\InitialPeople');
        $this->setPackage('Model.Custom.NovumCbs.Data');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('jaartal', 'Jaartal', 'INTEGER', true, null, null);
        $this->addColumn('mannen', 'Mannen', 'INTEGER', true, null, null);
        $this->addColumn('vrouwen', 'Vrouwen', 'INTEGER', true, null, null);
        $this->addColumn('leeftijd_0_20', 'Leeftijd020', 'INTEGER', true, null, null);
        $this->addColumn('leeftijd_20_45', 'Leeftijd2045', 'INTEGER', true, null, null);
        $this->addColumn('leeftijd_45_65', 'Leeftijd4565', 'INTEGER', true, null, null);
        $this->addColumn('leeftijd_65_80', 'Leeftijd6580', 'INTEGER', true, null, null);
        $this->addColumn('leeftijd_80_106', 'Leeftijd80106', 'INTEGER', true, null, null);
        $this->addColumn('landsdeel_noord', 'LandsdeelNoord', 'INTEGER', true, null, null);
        $this->addColumn('landsdeel_oost', 'LandsdeelOost', 'INTEGER', true, null, null);
        $this->addColumn('landsdeel_west', 'LandsdeelWest', 'INTEGER', true, null, null);
        $this->addColumn('landsdeel_zuid', 'LandsdeelZuid', 'INTEGER', true, null, null);
        $this->addColumn('burgerlijke_ongehuwd', 'BurgerlijkeOngehuwd', 'INTEGER', true, null, null);
        $this->addColumn('burgerlijke_gehuwd', 'BurgerlijkeGehuwd', 'INTEGER', true, null, null);
        $this->addColumn('burgerlijke_gescheiden', 'BurgerlijkeGescheiden', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? InitialPeopleTableMap::CLASS_DEFAULT : InitialPeopleTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (InitialPeople object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = InitialPeopleTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = InitialPeopleTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + InitialPeopleTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = InitialPeopleTableMap::OM_CLASS;
            /** @var InitialPeople $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            InitialPeopleTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = InitialPeopleTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = InitialPeopleTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var InitialPeople $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                InitialPeopleTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_ID);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_JAARTAL);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_MANNEN);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_VROUWEN);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LEEFTIJD_0_20);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LEEFTIJD_20_45);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LEEFTIJD_45_65);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LEEFTIJD_65_80);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LEEFTIJD_80_106);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LANDSDEEL_NOORD);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LANDSDEEL_OOST);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LANDSDEEL_WEST);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_LANDSDEEL_ZUID);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD);
            $criteria->addSelectColumn(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.jaartal');
            $criteria->addSelectColumn($alias . '.mannen');
            $criteria->addSelectColumn($alias . '.vrouwen');
            $criteria->addSelectColumn($alias . '.leeftijd_0_20');
            $criteria->addSelectColumn($alias . '.leeftijd_20_45');
            $criteria->addSelectColumn($alias . '.leeftijd_45_65');
            $criteria->addSelectColumn($alias . '.leeftijd_65_80');
            $criteria->addSelectColumn($alias . '.leeftijd_80_106');
            $criteria->addSelectColumn($alias . '.landsdeel_noord');
            $criteria->addSelectColumn($alias . '.landsdeel_oost');
            $criteria->addSelectColumn($alias . '.landsdeel_west');
            $criteria->addSelectColumn($alias . '.landsdeel_zuid');
            $criteria->addSelectColumn($alias . '.burgerlijke_ongehuwd');
            $criteria->addSelectColumn($alias . '.burgerlijke_gehuwd');
            $criteria->addSelectColumn($alias . '.burgerlijke_gescheiden');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(InitialPeopleTableMap::DATABASE_NAME)->getTable(InitialPeopleTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(InitialPeopleTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(InitialPeopleTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new InitialPeopleTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a InitialPeople or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or InitialPeople object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Custom\NovumCbs\Data\InitialPeople) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(InitialPeopleTableMap::DATABASE_NAME);
            $criteria->add(InitialPeopleTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = InitialPeopleQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            InitialPeopleTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                InitialPeopleTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the data_initial table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return InitialPeopleQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a InitialPeople or Criteria object.
     *
     * @param mixed               $criteria Criteria or InitialPeople object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from InitialPeople object
        }

        if ($criteria->containsKey(InitialPeopleTableMap::COL_ID) && $criteria->keyContainsValue(InitialPeopleTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.InitialPeopleTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = InitialPeopleQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // InitialPeopleTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
InitialPeopleTableMap::buildTableMap();

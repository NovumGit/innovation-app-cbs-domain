<?php

namespace Model\Custom\NovumCbs\Data\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumCbs\Data\InitialPeopleQuery as ChildInitialPeopleQuery;
use Model\Custom\NovumCbs\Data\Map\InitialPeopleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'data_initial' table.
 *
 *
 *
 * @package    propel.generator.Model.Custom.NovumCbs.Data.Base
 */
abstract class InitialPeople implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Custom\\NovumCbs\\Data\\Map\\InitialPeopleTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the jaartal field.
     *
     * @var        int
     */
    protected $jaartal;

    /**
     * The value for the mannen field.
     *
     * @var        int
     */
    protected $mannen;

    /**
     * The value for the vrouwen field.
     *
     * @var        int
     */
    protected $vrouwen;

    /**
     * The value for the leeftijd_0_20 field.
     *
     * @var        int
     */
    protected $leeftijd_0_20;

    /**
     * The value for the leeftijd_20_45 field.
     *
     * @var        int
     */
    protected $leeftijd_20_45;

    /**
     * The value for the leeftijd_45_65 field.
     *
     * @var        int
     */
    protected $leeftijd_45_65;

    /**
     * The value for the leeftijd_65_80 field.
     *
     * @var        int
     */
    protected $leeftijd_65_80;

    /**
     * The value for the leeftijd_80_106 field.
     *
     * @var        int
     */
    protected $leeftijd_80_106;

    /**
     * The value for the landsdeel_noord field.
     *
     * @var        int
     */
    protected $landsdeel_noord;

    /**
     * The value for the landsdeel_oost field.
     *
     * @var        int
     */
    protected $landsdeel_oost;

    /**
     * The value for the landsdeel_west field.
     *
     * @var        int
     */
    protected $landsdeel_west;

    /**
     * The value for the landsdeel_zuid field.
     *
     * @var        int
     */
    protected $landsdeel_zuid;

    /**
     * The value for the burgerlijke_ongehuwd field.
     *
     * @var        int
     */
    protected $burgerlijke_ongehuwd;

    /**
     * The value for the burgerlijke_gehuwd field.
     *
     * @var        int
     */
    protected $burgerlijke_gehuwd;

    /**
     * The value for the burgerlijke_gescheiden field.
     *
     * @var        int
     */
    protected $burgerlijke_gescheiden;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Model\Custom\NovumCbs\Data\Base\InitialPeople object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>InitialPeople</code> instance.  If
     * <code>obj</code> is an instance of <code>InitialPeople</code>, delegates to
     * <code>equals(InitialPeople)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|InitialPeople The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [jaartal] column value.
     *
     * @return int
     */
    public function getJaartal()
    {
        return $this->jaartal;
    }

    /**
     * Get the [mannen] column value.
     *
     * @return int
     */
    public function getMannen()
    {
        return $this->mannen;
    }

    /**
     * Get the [vrouwen] column value.
     *
     * @return int
     */
    public function getVrouwen()
    {
        return $this->vrouwen;
    }

    /**
     * Get the [leeftijd_0_20] column value.
     *
     * @return int
     */
    public function getLeeftijd020()
    {
        return $this->leeftijd_0_20;
    }

    /**
     * Get the [leeftijd_20_45] column value.
     *
     * @return int
     */
    public function getLeeftijd2045()
    {
        return $this->leeftijd_20_45;
    }

    /**
     * Get the [leeftijd_45_65] column value.
     *
     * @return int
     */
    public function getLeeftijd4565()
    {
        return $this->leeftijd_45_65;
    }

    /**
     * Get the [leeftijd_65_80] column value.
     *
     * @return int
     */
    public function getLeeftijd6580()
    {
        return $this->leeftijd_65_80;
    }

    /**
     * Get the [leeftijd_80_106] column value.
     *
     * @return int
     */
    public function getLeeftijd80106()
    {
        return $this->leeftijd_80_106;
    }

    /**
     * Get the [landsdeel_noord] column value.
     *
     * @return int
     */
    public function getLandsdeelNoord()
    {
        return $this->landsdeel_noord;
    }

    /**
     * Get the [landsdeel_oost] column value.
     *
     * @return int
     */
    public function getLandsdeelOost()
    {
        return $this->landsdeel_oost;
    }

    /**
     * Get the [landsdeel_west] column value.
     *
     * @return int
     */
    public function getLandsdeelWest()
    {
        return $this->landsdeel_west;
    }

    /**
     * Get the [landsdeel_zuid] column value.
     *
     * @return int
     */
    public function getLandsdeelZuid()
    {
        return $this->landsdeel_zuid;
    }

    /**
     * Get the [burgerlijke_ongehuwd] column value.
     *
     * @return int
     */
    public function getBurgerlijkeOngehuwd()
    {
        return $this->burgerlijke_ongehuwd;
    }

    /**
     * Get the [burgerlijke_gehuwd] column value.
     *
     * @return int
     */
    public function getBurgerlijkeGehuwd()
    {
        return $this->burgerlijke_gehuwd;
    }

    /**
     * Get the [burgerlijke_gescheiden] column value.
     *
     * @return int
     */
    public function getBurgerlijkeGescheiden()
    {
        return $this->burgerlijke_gescheiden;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [jaartal] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setJaartal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->jaartal !== $v) {
            $this->jaartal = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_JAARTAL] = true;
        }

        return $this;
    } // setJaartal()

    /**
     * Set the value of [mannen] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setMannen($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->mannen !== $v) {
            $this->mannen = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_MANNEN] = true;
        }

        return $this;
    } // setMannen()

    /**
     * Set the value of [vrouwen] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setVrouwen($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vrouwen !== $v) {
            $this->vrouwen = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_VROUWEN] = true;
        }

        return $this;
    } // setVrouwen()

    /**
     * Set the value of [leeftijd_0_20] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLeeftijd020($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->leeftijd_0_20 !== $v) {
            $this->leeftijd_0_20 = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LEEFTIJD_0_20] = true;
        }

        return $this;
    } // setLeeftijd020()

    /**
     * Set the value of [leeftijd_20_45] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLeeftijd2045($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->leeftijd_20_45 !== $v) {
            $this->leeftijd_20_45 = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LEEFTIJD_20_45] = true;
        }

        return $this;
    } // setLeeftijd2045()

    /**
     * Set the value of [leeftijd_45_65] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLeeftijd4565($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->leeftijd_45_65 !== $v) {
            $this->leeftijd_45_65 = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LEEFTIJD_45_65] = true;
        }

        return $this;
    } // setLeeftijd4565()

    /**
     * Set the value of [leeftijd_65_80] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLeeftijd6580($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->leeftijd_65_80 !== $v) {
            $this->leeftijd_65_80 = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LEEFTIJD_65_80] = true;
        }

        return $this;
    } // setLeeftijd6580()

    /**
     * Set the value of [leeftijd_80_106] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLeeftijd80106($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->leeftijd_80_106 !== $v) {
            $this->leeftijd_80_106 = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LEEFTIJD_80_106] = true;
        }

        return $this;
    } // setLeeftijd80106()

    /**
     * Set the value of [landsdeel_noord] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLandsdeelNoord($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->landsdeel_noord !== $v) {
            $this->landsdeel_noord = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LANDSDEEL_NOORD] = true;
        }

        return $this;
    } // setLandsdeelNoord()

    /**
     * Set the value of [landsdeel_oost] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLandsdeelOost($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->landsdeel_oost !== $v) {
            $this->landsdeel_oost = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LANDSDEEL_OOST] = true;
        }

        return $this;
    } // setLandsdeelOost()

    /**
     * Set the value of [landsdeel_west] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLandsdeelWest($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->landsdeel_west !== $v) {
            $this->landsdeel_west = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LANDSDEEL_WEST] = true;
        }

        return $this;
    } // setLandsdeelWest()

    /**
     * Set the value of [landsdeel_zuid] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setLandsdeelZuid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->landsdeel_zuid !== $v) {
            $this->landsdeel_zuid = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_LANDSDEEL_ZUID] = true;
        }

        return $this;
    } // setLandsdeelZuid()

    /**
     * Set the value of [burgerlijke_ongehuwd] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setBurgerlijkeOngehuwd($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->burgerlijke_ongehuwd !== $v) {
            $this->burgerlijke_ongehuwd = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD] = true;
        }

        return $this;
    } // setBurgerlijkeOngehuwd()

    /**
     * Set the value of [burgerlijke_gehuwd] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setBurgerlijkeGehuwd($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->burgerlijke_gehuwd !== $v) {
            $this->burgerlijke_gehuwd = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD] = true;
        }

        return $this;
    } // setBurgerlijkeGehuwd()

    /**
     * Set the value of [burgerlijke_gescheiden] column.
     *
     * @param int $v new value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object (for fluent API support)
     */
    public function setBurgerlijkeGescheiden($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->burgerlijke_gescheiden !== $v) {
            $this->burgerlijke_gescheiden = $v;
            $this->modifiedColumns[InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN] = true;
        }

        return $this;
    } // setBurgerlijkeGescheiden()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : InitialPeopleTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : InitialPeopleTableMap::translateFieldName('Jaartal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->jaartal = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : InitialPeopleTableMap::translateFieldName('Mannen', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mannen = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : InitialPeopleTableMap::translateFieldName('Vrouwen', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vrouwen = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : InitialPeopleTableMap::translateFieldName('Leeftijd020', TableMap::TYPE_PHPNAME, $indexType)];
            $this->leeftijd_0_20 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : InitialPeopleTableMap::translateFieldName('Leeftijd2045', TableMap::TYPE_PHPNAME, $indexType)];
            $this->leeftijd_20_45 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : InitialPeopleTableMap::translateFieldName('Leeftijd4565', TableMap::TYPE_PHPNAME, $indexType)];
            $this->leeftijd_45_65 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : InitialPeopleTableMap::translateFieldName('Leeftijd6580', TableMap::TYPE_PHPNAME, $indexType)];
            $this->leeftijd_65_80 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : InitialPeopleTableMap::translateFieldName('Leeftijd80106', TableMap::TYPE_PHPNAME, $indexType)];
            $this->leeftijd_80_106 = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : InitialPeopleTableMap::translateFieldName('LandsdeelNoord', TableMap::TYPE_PHPNAME, $indexType)];
            $this->landsdeel_noord = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : InitialPeopleTableMap::translateFieldName('LandsdeelOost', TableMap::TYPE_PHPNAME, $indexType)];
            $this->landsdeel_oost = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : InitialPeopleTableMap::translateFieldName('LandsdeelWest', TableMap::TYPE_PHPNAME, $indexType)];
            $this->landsdeel_west = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : InitialPeopleTableMap::translateFieldName('LandsdeelZuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->landsdeel_zuid = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : InitialPeopleTableMap::translateFieldName('BurgerlijkeOngehuwd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->burgerlijke_ongehuwd = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : InitialPeopleTableMap::translateFieldName('BurgerlijkeGehuwd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->burgerlijke_gehuwd = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : InitialPeopleTableMap::translateFieldName('BurgerlijkeGescheiden', TableMap::TYPE_PHPNAME, $indexType)];
            $this->burgerlijke_gescheiden = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = InitialPeopleTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Custom\\NovumCbs\\Data\\InitialPeople'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildInitialPeopleQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see InitialPeople::setDeleted()
     * @see InitialPeople::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildInitialPeopleQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                InitialPeopleTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[InitialPeopleTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . InitialPeopleTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(InitialPeopleTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_JAARTAL)) {
            $modifiedColumns[':p' . $index++]  = 'jaartal';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_MANNEN)) {
            $modifiedColumns[':p' . $index++]  = 'mannen';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_VROUWEN)) {
            $modifiedColumns[':p' . $index++]  = 'vrouwen';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_0_20)) {
            $modifiedColumns[':p' . $index++]  = 'leeftijd_0_20';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_20_45)) {
            $modifiedColumns[':p' . $index++]  = 'leeftijd_20_45';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_45_65)) {
            $modifiedColumns[':p' . $index++]  = 'leeftijd_45_65';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_65_80)) {
            $modifiedColumns[':p' . $index++]  = 'leeftijd_65_80';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_80_106)) {
            $modifiedColumns[':p' . $index++]  = 'leeftijd_80_106';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_NOORD)) {
            $modifiedColumns[':p' . $index++]  = 'landsdeel_noord';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_OOST)) {
            $modifiedColumns[':p' . $index++]  = 'landsdeel_oost';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_WEST)) {
            $modifiedColumns[':p' . $index++]  = 'landsdeel_west';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_ZUID)) {
            $modifiedColumns[':p' . $index++]  = 'landsdeel_zuid';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD)) {
            $modifiedColumns[':p' . $index++]  = 'burgerlijke_ongehuwd';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD)) {
            $modifiedColumns[':p' . $index++]  = 'burgerlijke_gehuwd';
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN)) {
            $modifiedColumns[':p' . $index++]  = 'burgerlijke_gescheiden';
        }

        $sql = sprintf(
            'INSERT INTO data_initial (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'jaartal':
                        $stmt->bindValue($identifier, $this->jaartal, PDO::PARAM_INT);
                        break;
                    case 'mannen':
                        $stmt->bindValue($identifier, $this->mannen, PDO::PARAM_INT);
                        break;
                    case 'vrouwen':
                        $stmt->bindValue($identifier, $this->vrouwen, PDO::PARAM_INT);
                        break;
                    case 'leeftijd_0_20':
                        $stmt->bindValue($identifier, $this->leeftijd_0_20, PDO::PARAM_INT);
                        break;
                    case 'leeftijd_20_45':
                        $stmt->bindValue($identifier, $this->leeftijd_20_45, PDO::PARAM_INT);
                        break;
                    case 'leeftijd_45_65':
                        $stmt->bindValue($identifier, $this->leeftijd_45_65, PDO::PARAM_INT);
                        break;
                    case 'leeftijd_65_80':
                        $stmt->bindValue($identifier, $this->leeftijd_65_80, PDO::PARAM_INT);
                        break;
                    case 'leeftijd_80_106':
                        $stmt->bindValue($identifier, $this->leeftijd_80_106, PDO::PARAM_INT);
                        break;
                    case 'landsdeel_noord':
                        $stmt->bindValue($identifier, $this->landsdeel_noord, PDO::PARAM_INT);
                        break;
                    case 'landsdeel_oost':
                        $stmt->bindValue($identifier, $this->landsdeel_oost, PDO::PARAM_INT);
                        break;
                    case 'landsdeel_west':
                        $stmt->bindValue($identifier, $this->landsdeel_west, PDO::PARAM_INT);
                        break;
                    case 'landsdeel_zuid':
                        $stmt->bindValue($identifier, $this->landsdeel_zuid, PDO::PARAM_INT);
                        break;
                    case 'burgerlijke_ongehuwd':
                        $stmt->bindValue($identifier, $this->burgerlijke_ongehuwd, PDO::PARAM_INT);
                        break;
                    case 'burgerlijke_gehuwd':
                        $stmt->bindValue($identifier, $this->burgerlijke_gehuwd, PDO::PARAM_INT);
                        break;
                    case 'burgerlijke_gescheiden':
                        $stmt->bindValue($identifier, $this->burgerlijke_gescheiden, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = InitialPeopleTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getJaartal();
                break;
            case 2:
                return $this->getMannen();
                break;
            case 3:
                return $this->getVrouwen();
                break;
            case 4:
                return $this->getLeeftijd020();
                break;
            case 5:
                return $this->getLeeftijd2045();
                break;
            case 6:
                return $this->getLeeftijd4565();
                break;
            case 7:
                return $this->getLeeftijd6580();
                break;
            case 8:
                return $this->getLeeftijd80106();
                break;
            case 9:
                return $this->getLandsdeelNoord();
                break;
            case 10:
                return $this->getLandsdeelOost();
                break;
            case 11:
                return $this->getLandsdeelWest();
                break;
            case 12:
                return $this->getLandsdeelZuid();
                break;
            case 13:
                return $this->getBurgerlijkeOngehuwd();
                break;
            case 14:
                return $this->getBurgerlijkeGehuwd();
                break;
            case 15:
                return $this->getBurgerlijkeGescheiden();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['InitialPeople'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['InitialPeople'][$this->hashCode()] = true;
        $keys = InitialPeopleTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getJaartal(),
            $keys[2] => $this->getMannen(),
            $keys[3] => $this->getVrouwen(),
            $keys[4] => $this->getLeeftijd020(),
            $keys[5] => $this->getLeeftijd2045(),
            $keys[6] => $this->getLeeftijd4565(),
            $keys[7] => $this->getLeeftijd6580(),
            $keys[8] => $this->getLeeftijd80106(),
            $keys[9] => $this->getLandsdeelNoord(),
            $keys[10] => $this->getLandsdeelOost(),
            $keys[11] => $this->getLandsdeelWest(),
            $keys[12] => $this->getLandsdeelZuid(),
            $keys[13] => $this->getBurgerlijkeOngehuwd(),
            $keys[14] => $this->getBurgerlijkeGehuwd(),
            $keys[15] => $this->getBurgerlijkeGescheiden(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = InitialPeopleTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setJaartal($value);
                break;
            case 2:
                $this->setMannen($value);
                break;
            case 3:
                $this->setVrouwen($value);
                break;
            case 4:
                $this->setLeeftijd020($value);
                break;
            case 5:
                $this->setLeeftijd2045($value);
                break;
            case 6:
                $this->setLeeftijd4565($value);
                break;
            case 7:
                $this->setLeeftijd6580($value);
                break;
            case 8:
                $this->setLeeftijd80106($value);
                break;
            case 9:
                $this->setLandsdeelNoord($value);
                break;
            case 10:
                $this->setLandsdeelOost($value);
                break;
            case 11:
                $this->setLandsdeelWest($value);
                break;
            case 12:
                $this->setLandsdeelZuid($value);
                break;
            case 13:
                $this->setBurgerlijkeOngehuwd($value);
                break;
            case 14:
                $this->setBurgerlijkeGehuwd($value);
                break;
            case 15:
                $this->setBurgerlijkeGescheiden($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = InitialPeopleTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setJaartal($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMannen($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setVrouwen($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setLeeftijd020($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLeeftijd2045($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setLeeftijd4565($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setLeeftijd6580($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLeeftijd80106($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setLandsdeelNoord($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLandsdeelOost($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setLandsdeelWest($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setLandsdeelZuid($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setBurgerlijkeOngehuwd($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setBurgerlijkeGehuwd($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setBurgerlijkeGescheiden($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Custom\NovumCbs\Data\InitialPeople The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(InitialPeopleTableMap::DATABASE_NAME);

        if ($this->isColumnModified(InitialPeopleTableMap::COL_ID)) {
            $criteria->add(InitialPeopleTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_JAARTAL)) {
            $criteria->add(InitialPeopleTableMap::COL_JAARTAL, $this->jaartal);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_MANNEN)) {
            $criteria->add(InitialPeopleTableMap::COL_MANNEN, $this->mannen);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_VROUWEN)) {
            $criteria->add(InitialPeopleTableMap::COL_VROUWEN, $this->vrouwen);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_0_20)) {
            $criteria->add(InitialPeopleTableMap::COL_LEEFTIJD_0_20, $this->leeftijd_0_20);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_20_45)) {
            $criteria->add(InitialPeopleTableMap::COL_LEEFTIJD_20_45, $this->leeftijd_20_45);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_45_65)) {
            $criteria->add(InitialPeopleTableMap::COL_LEEFTIJD_45_65, $this->leeftijd_45_65);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_65_80)) {
            $criteria->add(InitialPeopleTableMap::COL_LEEFTIJD_65_80, $this->leeftijd_65_80);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LEEFTIJD_80_106)) {
            $criteria->add(InitialPeopleTableMap::COL_LEEFTIJD_80_106, $this->leeftijd_80_106);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_NOORD)) {
            $criteria->add(InitialPeopleTableMap::COL_LANDSDEEL_NOORD, $this->landsdeel_noord);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_OOST)) {
            $criteria->add(InitialPeopleTableMap::COL_LANDSDEEL_OOST, $this->landsdeel_oost);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_WEST)) {
            $criteria->add(InitialPeopleTableMap::COL_LANDSDEEL_WEST, $this->landsdeel_west);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_LANDSDEEL_ZUID)) {
            $criteria->add(InitialPeopleTableMap::COL_LANDSDEEL_ZUID, $this->landsdeel_zuid);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD)) {
            $criteria->add(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD, $this->burgerlijke_ongehuwd);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD)) {
            $criteria->add(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD, $this->burgerlijke_gehuwd);
        }
        if ($this->isColumnModified(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN)) {
            $criteria->add(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN, $this->burgerlijke_gescheiden);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildInitialPeopleQuery::create();
        $criteria->add(InitialPeopleTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Custom\NovumCbs\Data\InitialPeople (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setJaartal($this->getJaartal());
        $copyObj->setMannen($this->getMannen());
        $copyObj->setVrouwen($this->getVrouwen());
        $copyObj->setLeeftijd020($this->getLeeftijd020());
        $copyObj->setLeeftijd2045($this->getLeeftijd2045());
        $copyObj->setLeeftijd4565($this->getLeeftijd4565());
        $copyObj->setLeeftijd6580($this->getLeeftijd6580());
        $copyObj->setLeeftijd80106($this->getLeeftijd80106());
        $copyObj->setLandsdeelNoord($this->getLandsdeelNoord());
        $copyObj->setLandsdeelOost($this->getLandsdeelOost());
        $copyObj->setLandsdeelWest($this->getLandsdeelWest());
        $copyObj->setLandsdeelZuid($this->getLandsdeelZuid());
        $copyObj->setBurgerlijkeOngehuwd($this->getBurgerlijkeOngehuwd());
        $copyObj->setBurgerlijkeGehuwd($this->getBurgerlijkeGehuwd());
        $copyObj->setBurgerlijkeGescheiden($this->getBurgerlijkeGescheiden());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Custom\NovumCbs\Data\InitialPeople Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->jaartal = null;
        $this->mannen = null;
        $this->vrouwen = null;
        $this->leeftijd_0_20 = null;
        $this->leeftijd_20_45 = null;
        $this->leeftijd_45_65 = null;
        $this->leeftijd_65_80 = null;
        $this->leeftijd_80_106 = null;
        $this->landsdeel_noord = null;
        $this->landsdeel_oost = null;
        $this->landsdeel_west = null;
        $this->landsdeel_zuid = null;
        $this->burgerlijke_ongehuwd = null;
        $this->burgerlijke_gehuwd = null;
        $this->burgerlijke_gescheiden = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(InitialPeopleTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace Model\Custom\NovumCbs\Data\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumCbs\Data\InitialPeople as ChildInitialPeople;
use Model\Custom\NovumCbs\Data\InitialPeopleQuery as ChildInitialPeopleQuery;
use Model\Custom\NovumCbs\Data\Map\InitialPeopleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'data_initial' table.
 *
 *
 *
 * @method     ChildInitialPeopleQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildInitialPeopleQuery orderByJaartal($order = Criteria::ASC) Order by the jaartal column
 * @method     ChildInitialPeopleQuery orderByMannen($order = Criteria::ASC) Order by the mannen column
 * @method     ChildInitialPeopleQuery orderByVrouwen($order = Criteria::ASC) Order by the vrouwen column
 * @method     ChildInitialPeopleQuery orderByLeeftijd020($order = Criteria::ASC) Order by the leeftijd_0_20 column
 * @method     ChildInitialPeopleQuery orderByLeeftijd2045($order = Criteria::ASC) Order by the leeftijd_20_45 column
 * @method     ChildInitialPeopleQuery orderByLeeftijd4565($order = Criteria::ASC) Order by the leeftijd_45_65 column
 * @method     ChildInitialPeopleQuery orderByLeeftijd6580($order = Criteria::ASC) Order by the leeftijd_65_80 column
 * @method     ChildInitialPeopleQuery orderByLeeftijd80106($order = Criteria::ASC) Order by the leeftijd_80_106 column
 * @method     ChildInitialPeopleQuery orderByLandsdeelNoord($order = Criteria::ASC) Order by the landsdeel_noord column
 * @method     ChildInitialPeopleQuery orderByLandsdeelOost($order = Criteria::ASC) Order by the landsdeel_oost column
 * @method     ChildInitialPeopleQuery orderByLandsdeelWest($order = Criteria::ASC) Order by the landsdeel_west column
 * @method     ChildInitialPeopleQuery orderByLandsdeelZuid($order = Criteria::ASC) Order by the landsdeel_zuid column
 * @method     ChildInitialPeopleQuery orderByBurgerlijkeOngehuwd($order = Criteria::ASC) Order by the burgerlijke_ongehuwd column
 * @method     ChildInitialPeopleQuery orderByBurgerlijkeGehuwd($order = Criteria::ASC) Order by the burgerlijke_gehuwd column
 * @method     ChildInitialPeopleQuery orderByBurgerlijkeGescheiden($order = Criteria::ASC) Order by the burgerlijke_gescheiden column
 *
 * @method     ChildInitialPeopleQuery groupById() Group by the id column
 * @method     ChildInitialPeopleQuery groupByJaartal() Group by the jaartal column
 * @method     ChildInitialPeopleQuery groupByMannen() Group by the mannen column
 * @method     ChildInitialPeopleQuery groupByVrouwen() Group by the vrouwen column
 * @method     ChildInitialPeopleQuery groupByLeeftijd020() Group by the leeftijd_0_20 column
 * @method     ChildInitialPeopleQuery groupByLeeftijd2045() Group by the leeftijd_20_45 column
 * @method     ChildInitialPeopleQuery groupByLeeftijd4565() Group by the leeftijd_45_65 column
 * @method     ChildInitialPeopleQuery groupByLeeftijd6580() Group by the leeftijd_65_80 column
 * @method     ChildInitialPeopleQuery groupByLeeftijd80106() Group by the leeftijd_80_106 column
 * @method     ChildInitialPeopleQuery groupByLandsdeelNoord() Group by the landsdeel_noord column
 * @method     ChildInitialPeopleQuery groupByLandsdeelOost() Group by the landsdeel_oost column
 * @method     ChildInitialPeopleQuery groupByLandsdeelWest() Group by the landsdeel_west column
 * @method     ChildInitialPeopleQuery groupByLandsdeelZuid() Group by the landsdeel_zuid column
 * @method     ChildInitialPeopleQuery groupByBurgerlijkeOngehuwd() Group by the burgerlijke_ongehuwd column
 * @method     ChildInitialPeopleQuery groupByBurgerlijkeGehuwd() Group by the burgerlijke_gehuwd column
 * @method     ChildInitialPeopleQuery groupByBurgerlijkeGescheiden() Group by the burgerlijke_gescheiden column
 *
 * @method     ChildInitialPeopleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildInitialPeopleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildInitialPeopleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildInitialPeopleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildInitialPeopleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildInitialPeopleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildInitialPeople findOne(ConnectionInterface $con = null) Return the first ChildInitialPeople matching the query
 * @method     ChildInitialPeople findOneOrCreate(ConnectionInterface $con = null) Return the first ChildInitialPeople matching the query, or a new ChildInitialPeople object populated from the query conditions when no match is found
 *
 * @method     ChildInitialPeople findOneById(int $id) Return the first ChildInitialPeople filtered by the id column
 * @method     ChildInitialPeople findOneByJaartal(int $jaartal) Return the first ChildInitialPeople filtered by the jaartal column
 * @method     ChildInitialPeople findOneByMannen(int $mannen) Return the first ChildInitialPeople filtered by the mannen column
 * @method     ChildInitialPeople findOneByVrouwen(int $vrouwen) Return the first ChildInitialPeople filtered by the vrouwen column
 * @method     ChildInitialPeople findOneByLeeftijd020(int $leeftijd_0_20) Return the first ChildInitialPeople filtered by the leeftijd_0_20 column
 * @method     ChildInitialPeople findOneByLeeftijd2045(int $leeftijd_20_45) Return the first ChildInitialPeople filtered by the leeftijd_20_45 column
 * @method     ChildInitialPeople findOneByLeeftijd4565(int $leeftijd_45_65) Return the first ChildInitialPeople filtered by the leeftijd_45_65 column
 * @method     ChildInitialPeople findOneByLeeftijd6580(int $leeftijd_65_80) Return the first ChildInitialPeople filtered by the leeftijd_65_80 column
 * @method     ChildInitialPeople findOneByLeeftijd80106(int $leeftijd_80_106) Return the first ChildInitialPeople filtered by the leeftijd_80_106 column
 * @method     ChildInitialPeople findOneByLandsdeelNoord(int $landsdeel_noord) Return the first ChildInitialPeople filtered by the landsdeel_noord column
 * @method     ChildInitialPeople findOneByLandsdeelOost(int $landsdeel_oost) Return the first ChildInitialPeople filtered by the landsdeel_oost column
 * @method     ChildInitialPeople findOneByLandsdeelWest(int $landsdeel_west) Return the first ChildInitialPeople filtered by the landsdeel_west column
 * @method     ChildInitialPeople findOneByLandsdeelZuid(int $landsdeel_zuid) Return the first ChildInitialPeople filtered by the landsdeel_zuid column
 * @method     ChildInitialPeople findOneByBurgerlijkeOngehuwd(int $burgerlijke_ongehuwd) Return the first ChildInitialPeople filtered by the burgerlijke_ongehuwd column
 * @method     ChildInitialPeople findOneByBurgerlijkeGehuwd(int $burgerlijke_gehuwd) Return the first ChildInitialPeople filtered by the burgerlijke_gehuwd column
 * @method     ChildInitialPeople findOneByBurgerlijkeGescheiden(int $burgerlijke_gescheiden) Return the first ChildInitialPeople filtered by the burgerlijke_gescheiden column *

 * @method     ChildInitialPeople requirePk($key, ConnectionInterface $con = null) Return the ChildInitialPeople by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOne(ConnectionInterface $con = null) Return the first ChildInitialPeople matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInitialPeople requireOneById(int $id) Return the first ChildInitialPeople filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByJaartal(int $jaartal) Return the first ChildInitialPeople filtered by the jaartal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByMannen(int $mannen) Return the first ChildInitialPeople filtered by the mannen column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByVrouwen(int $vrouwen) Return the first ChildInitialPeople filtered by the vrouwen column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLeeftijd020(int $leeftijd_0_20) Return the first ChildInitialPeople filtered by the leeftijd_0_20 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLeeftijd2045(int $leeftijd_20_45) Return the first ChildInitialPeople filtered by the leeftijd_20_45 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLeeftijd4565(int $leeftijd_45_65) Return the first ChildInitialPeople filtered by the leeftijd_45_65 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLeeftijd6580(int $leeftijd_65_80) Return the first ChildInitialPeople filtered by the leeftijd_65_80 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLeeftijd80106(int $leeftijd_80_106) Return the first ChildInitialPeople filtered by the leeftijd_80_106 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLandsdeelNoord(int $landsdeel_noord) Return the first ChildInitialPeople filtered by the landsdeel_noord column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLandsdeelOost(int $landsdeel_oost) Return the first ChildInitialPeople filtered by the landsdeel_oost column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLandsdeelWest(int $landsdeel_west) Return the first ChildInitialPeople filtered by the landsdeel_west column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByLandsdeelZuid(int $landsdeel_zuid) Return the first ChildInitialPeople filtered by the landsdeel_zuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByBurgerlijkeOngehuwd(int $burgerlijke_ongehuwd) Return the first ChildInitialPeople filtered by the burgerlijke_ongehuwd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByBurgerlijkeGehuwd(int $burgerlijke_gehuwd) Return the first ChildInitialPeople filtered by the burgerlijke_gehuwd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInitialPeople requireOneByBurgerlijkeGescheiden(int $burgerlijke_gescheiden) Return the first ChildInitialPeople filtered by the burgerlijke_gescheiden column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInitialPeople[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildInitialPeople objects based on current ModelCriteria
 * @method     ChildInitialPeople[]|ObjectCollection findById(int $id) Return ChildInitialPeople objects filtered by the id column
 * @method     ChildInitialPeople[]|ObjectCollection findByJaartal(int $jaartal) Return ChildInitialPeople objects filtered by the jaartal column
 * @method     ChildInitialPeople[]|ObjectCollection findByMannen(int $mannen) Return ChildInitialPeople objects filtered by the mannen column
 * @method     ChildInitialPeople[]|ObjectCollection findByVrouwen(int $vrouwen) Return ChildInitialPeople objects filtered by the vrouwen column
 * @method     ChildInitialPeople[]|ObjectCollection findByLeeftijd020(int $leeftijd_0_20) Return ChildInitialPeople objects filtered by the leeftijd_0_20 column
 * @method     ChildInitialPeople[]|ObjectCollection findByLeeftijd2045(int $leeftijd_20_45) Return ChildInitialPeople objects filtered by the leeftijd_20_45 column
 * @method     ChildInitialPeople[]|ObjectCollection findByLeeftijd4565(int $leeftijd_45_65) Return ChildInitialPeople objects filtered by the leeftijd_45_65 column
 * @method     ChildInitialPeople[]|ObjectCollection findByLeeftijd6580(int $leeftijd_65_80) Return ChildInitialPeople objects filtered by the leeftijd_65_80 column
 * @method     ChildInitialPeople[]|ObjectCollection findByLeeftijd80106(int $leeftijd_80_106) Return ChildInitialPeople objects filtered by the leeftijd_80_106 column
 * @method     ChildInitialPeople[]|ObjectCollection findByLandsdeelNoord(int $landsdeel_noord) Return ChildInitialPeople objects filtered by the landsdeel_noord column
 * @method     ChildInitialPeople[]|ObjectCollection findByLandsdeelOost(int $landsdeel_oost) Return ChildInitialPeople objects filtered by the landsdeel_oost column
 * @method     ChildInitialPeople[]|ObjectCollection findByLandsdeelWest(int $landsdeel_west) Return ChildInitialPeople objects filtered by the landsdeel_west column
 * @method     ChildInitialPeople[]|ObjectCollection findByLandsdeelZuid(int $landsdeel_zuid) Return ChildInitialPeople objects filtered by the landsdeel_zuid column
 * @method     ChildInitialPeople[]|ObjectCollection findByBurgerlijkeOngehuwd(int $burgerlijke_ongehuwd) Return ChildInitialPeople objects filtered by the burgerlijke_ongehuwd column
 * @method     ChildInitialPeople[]|ObjectCollection findByBurgerlijkeGehuwd(int $burgerlijke_gehuwd) Return ChildInitialPeople objects filtered by the burgerlijke_gehuwd column
 * @method     ChildInitialPeople[]|ObjectCollection findByBurgerlijkeGescheiden(int $burgerlijke_gescheiden) Return ChildInitialPeople objects filtered by the burgerlijke_gescheiden column
 * @method     ChildInitialPeople[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class InitialPeopleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Custom\NovumCbs\Data\Base\InitialPeopleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Custom\\NovumCbs\\Data\\InitialPeople', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildInitialPeopleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildInitialPeopleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildInitialPeopleQuery) {
            return $criteria;
        }
        $query = new ChildInitialPeopleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildInitialPeople|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = InitialPeopleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildInitialPeople A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, jaartal, mannen, vrouwen, leeftijd_0_20, leeftijd_20_45, leeftijd_45_65, leeftijd_65_80, leeftijd_80_106, landsdeel_noord, landsdeel_oost, landsdeel_west, landsdeel_zuid, burgerlijke_ongehuwd, burgerlijke_gehuwd, burgerlijke_gescheiden FROM data_initial WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildInitialPeople $obj */
            $obj = new ChildInitialPeople();
            $obj->hydrate($row);
            InitialPeopleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildInitialPeople|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InitialPeopleTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InitialPeopleTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the jaartal column
     *
     * Example usage:
     * <code>
     * $query->filterByJaartal(1234); // WHERE jaartal = 1234
     * $query->filterByJaartal(array(12, 34)); // WHERE jaartal IN (12, 34)
     * $query->filterByJaartal(array('min' => 12)); // WHERE jaartal > 12
     * </code>
     *
     * @param     mixed $jaartal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByJaartal($jaartal = null, $comparison = null)
    {
        if (is_array($jaartal)) {
            $useMinMax = false;
            if (isset($jaartal['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_JAARTAL, $jaartal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jaartal['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_JAARTAL, $jaartal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_JAARTAL, $jaartal, $comparison);
    }

    /**
     * Filter the query on the mannen column
     *
     * Example usage:
     * <code>
     * $query->filterByMannen(1234); // WHERE mannen = 1234
     * $query->filterByMannen(array(12, 34)); // WHERE mannen IN (12, 34)
     * $query->filterByMannen(array('min' => 12)); // WHERE mannen > 12
     * </code>
     *
     * @param     mixed $mannen The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByMannen($mannen = null, $comparison = null)
    {
        if (is_array($mannen)) {
            $useMinMax = false;
            if (isset($mannen['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_MANNEN, $mannen['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mannen['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_MANNEN, $mannen['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_MANNEN, $mannen, $comparison);
    }

    /**
     * Filter the query on the vrouwen column
     *
     * Example usage:
     * <code>
     * $query->filterByVrouwen(1234); // WHERE vrouwen = 1234
     * $query->filterByVrouwen(array(12, 34)); // WHERE vrouwen IN (12, 34)
     * $query->filterByVrouwen(array('min' => 12)); // WHERE vrouwen > 12
     * </code>
     *
     * @param     mixed $vrouwen The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByVrouwen($vrouwen = null, $comparison = null)
    {
        if (is_array($vrouwen)) {
            $useMinMax = false;
            if (isset($vrouwen['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_VROUWEN, $vrouwen['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vrouwen['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_VROUWEN, $vrouwen['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_VROUWEN, $vrouwen, $comparison);
    }

    /**
     * Filter the query on the leeftijd_0_20 column
     *
     * Example usage:
     * <code>
     * $query->filterByLeeftijd020(1234); // WHERE leeftijd_0_20 = 1234
     * $query->filterByLeeftijd020(array(12, 34)); // WHERE leeftijd_0_20 IN (12, 34)
     * $query->filterByLeeftijd020(array('min' => 12)); // WHERE leeftijd_0_20 > 12
     * </code>
     *
     * @param     mixed $leeftijd020 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLeeftijd020($leeftijd020 = null, $comparison = null)
    {
        if (is_array($leeftijd020)) {
            $useMinMax = false;
            if (isset($leeftijd020['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_0_20, $leeftijd020['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leeftijd020['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_0_20, $leeftijd020['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_0_20, $leeftijd020, $comparison);
    }

    /**
     * Filter the query on the leeftijd_20_45 column
     *
     * Example usage:
     * <code>
     * $query->filterByLeeftijd2045(1234); // WHERE leeftijd_20_45 = 1234
     * $query->filterByLeeftijd2045(array(12, 34)); // WHERE leeftijd_20_45 IN (12, 34)
     * $query->filterByLeeftijd2045(array('min' => 12)); // WHERE leeftijd_20_45 > 12
     * </code>
     *
     * @param     mixed $leeftijd2045 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLeeftijd2045($leeftijd2045 = null, $comparison = null)
    {
        if (is_array($leeftijd2045)) {
            $useMinMax = false;
            if (isset($leeftijd2045['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_20_45, $leeftijd2045['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leeftijd2045['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_20_45, $leeftijd2045['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_20_45, $leeftijd2045, $comparison);
    }

    /**
     * Filter the query on the leeftijd_45_65 column
     *
     * Example usage:
     * <code>
     * $query->filterByLeeftijd4565(1234); // WHERE leeftijd_45_65 = 1234
     * $query->filterByLeeftijd4565(array(12, 34)); // WHERE leeftijd_45_65 IN (12, 34)
     * $query->filterByLeeftijd4565(array('min' => 12)); // WHERE leeftijd_45_65 > 12
     * </code>
     *
     * @param     mixed $leeftijd4565 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLeeftijd4565($leeftijd4565 = null, $comparison = null)
    {
        if (is_array($leeftijd4565)) {
            $useMinMax = false;
            if (isset($leeftijd4565['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_45_65, $leeftijd4565['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leeftijd4565['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_45_65, $leeftijd4565['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_45_65, $leeftijd4565, $comparison);
    }

    /**
     * Filter the query on the leeftijd_65_80 column
     *
     * Example usage:
     * <code>
     * $query->filterByLeeftijd6580(1234); // WHERE leeftijd_65_80 = 1234
     * $query->filterByLeeftijd6580(array(12, 34)); // WHERE leeftijd_65_80 IN (12, 34)
     * $query->filterByLeeftijd6580(array('min' => 12)); // WHERE leeftijd_65_80 > 12
     * </code>
     *
     * @param     mixed $leeftijd6580 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLeeftijd6580($leeftijd6580 = null, $comparison = null)
    {
        if (is_array($leeftijd6580)) {
            $useMinMax = false;
            if (isset($leeftijd6580['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_65_80, $leeftijd6580['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leeftijd6580['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_65_80, $leeftijd6580['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_65_80, $leeftijd6580, $comparison);
    }

    /**
     * Filter the query on the leeftijd_80_106 column
     *
     * Example usage:
     * <code>
     * $query->filterByLeeftijd80106(1234); // WHERE leeftijd_80_106 = 1234
     * $query->filterByLeeftijd80106(array(12, 34)); // WHERE leeftijd_80_106 IN (12, 34)
     * $query->filterByLeeftijd80106(array('min' => 12)); // WHERE leeftijd_80_106 > 12
     * </code>
     *
     * @param     mixed $leeftijd80106 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLeeftijd80106($leeftijd80106 = null, $comparison = null)
    {
        if (is_array($leeftijd80106)) {
            $useMinMax = false;
            if (isset($leeftijd80106['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_80_106, $leeftijd80106['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($leeftijd80106['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_80_106, $leeftijd80106['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LEEFTIJD_80_106, $leeftijd80106, $comparison);
    }

    /**
     * Filter the query on the landsdeel_noord column
     *
     * Example usage:
     * <code>
     * $query->filterByLandsdeelNoord(1234); // WHERE landsdeel_noord = 1234
     * $query->filterByLandsdeelNoord(array(12, 34)); // WHERE landsdeel_noord IN (12, 34)
     * $query->filterByLandsdeelNoord(array('min' => 12)); // WHERE landsdeel_noord > 12
     * </code>
     *
     * @param     mixed $landsdeelNoord The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLandsdeelNoord($landsdeelNoord = null, $comparison = null)
    {
        if (is_array($landsdeelNoord)) {
            $useMinMax = false;
            if (isset($landsdeelNoord['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_NOORD, $landsdeelNoord['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($landsdeelNoord['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_NOORD, $landsdeelNoord['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_NOORD, $landsdeelNoord, $comparison);
    }

    /**
     * Filter the query on the landsdeel_oost column
     *
     * Example usage:
     * <code>
     * $query->filterByLandsdeelOost(1234); // WHERE landsdeel_oost = 1234
     * $query->filterByLandsdeelOost(array(12, 34)); // WHERE landsdeel_oost IN (12, 34)
     * $query->filterByLandsdeelOost(array('min' => 12)); // WHERE landsdeel_oost > 12
     * </code>
     *
     * @param     mixed $landsdeelOost The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLandsdeelOost($landsdeelOost = null, $comparison = null)
    {
        if (is_array($landsdeelOost)) {
            $useMinMax = false;
            if (isset($landsdeelOost['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_OOST, $landsdeelOost['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($landsdeelOost['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_OOST, $landsdeelOost['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_OOST, $landsdeelOost, $comparison);
    }

    /**
     * Filter the query on the landsdeel_west column
     *
     * Example usage:
     * <code>
     * $query->filterByLandsdeelWest(1234); // WHERE landsdeel_west = 1234
     * $query->filterByLandsdeelWest(array(12, 34)); // WHERE landsdeel_west IN (12, 34)
     * $query->filterByLandsdeelWest(array('min' => 12)); // WHERE landsdeel_west > 12
     * </code>
     *
     * @param     mixed $landsdeelWest The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLandsdeelWest($landsdeelWest = null, $comparison = null)
    {
        if (is_array($landsdeelWest)) {
            $useMinMax = false;
            if (isset($landsdeelWest['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_WEST, $landsdeelWest['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($landsdeelWest['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_WEST, $landsdeelWest['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_WEST, $landsdeelWest, $comparison);
    }

    /**
     * Filter the query on the landsdeel_zuid column
     *
     * Example usage:
     * <code>
     * $query->filterByLandsdeelZuid(1234); // WHERE landsdeel_zuid = 1234
     * $query->filterByLandsdeelZuid(array(12, 34)); // WHERE landsdeel_zuid IN (12, 34)
     * $query->filterByLandsdeelZuid(array('min' => 12)); // WHERE landsdeel_zuid > 12
     * </code>
     *
     * @param     mixed $landsdeelZuid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByLandsdeelZuid($landsdeelZuid = null, $comparison = null)
    {
        if (is_array($landsdeelZuid)) {
            $useMinMax = false;
            if (isset($landsdeelZuid['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_ZUID, $landsdeelZuid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($landsdeelZuid['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_ZUID, $landsdeelZuid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_LANDSDEEL_ZUID, $landsdeelZuid, $comparison);
    }

    /**
     * Filter the query on the burgerlijke_ongehuwd column
     *
     * Example usage:
     * <code>
     * $query->filterByBurgerlijkeOngehuwd(1234); // WHERE burgerlijke_ongehuwd = 1234
     * $query->filterByBurgerlijkeOngehuwd(array(12, 34)); // WHERE burgerlijke_ongehuwd IN (12, 34)
     * $query->filterByBurgerlijkeOngehuwd(array('min' => 12)); // WHERE burgerlijke_ongehuwd > 12
     * </code>
     *
     * @param     mixed $burgerlijkeOngehuwd The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByBurgerlijkeOngehuwd($burgerlijkeOngehuwd = null, $comparison = null)
    {
        if (is_array($burgerlijkeOngehuwd)) {
            $useMinMax = false;
            if (isset($burgerlijkeOngehuwd['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD, $burgerlijkeOngehuwd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($burgerlijkeOngehuwd['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD, $burgerlijkeOngehuwd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_ONGEHUWD, $burgerlijkeOngehuwd, $comparison);
    }

    /**
     * Filter the query on the burgerlijke_gehuwd column
     *
     * Example usage:
     * <code>
     * $query->filterByBurgerlijkeGehuwd(1234); // WHERE burgerlijke_gehuwd = 1234
     * $query->filterByBurgerlijkeGehuwd(array(12, 34)); // WHERE burgerlijke_gehuwd IN (12, 34)
     * $query->filterByBurgerlijkeGehuwd(array('min' => 12)); // WHERE burgerlijke_gehuwd > 12
     * </code>
     *
     * @param     mixed $burgerlijkeGehuwd The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByBurgerlijkeGehuwd($burgerlijkeGehuwd = null, $comparison = null)
    {
        if (is_array($burgerlijkeGehuwd)) {
            $useMinMax = false;
            if (isset($burgerlijkeGehuwd['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD, $burgerlijkeGehuwd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($burgerlijkeGehuwd['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD, $burgerlijkeGehuwd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_GEHUWD, $burgerlijkeGehuwd, $comparison);
    }

    /**
     * Filter the query on the burgerlijke_gescheiden column
     *
     * Example usage:
     * <code>
     * $query->filterByBurgerlijkeGescheiden(1234); // WHERE burgerlijke_gescheiden = 1234
     * $query->filterByBurgerlijkeGescheiden(array(12, 34)); // WHERE burgerlijke_gescheiden IN (12, 34)
     * $query->filterByBurgerlijkeGescheiden(array('min' => 12)); // WHERE burgerlijke_gescheiden > 12
     * </code>
     *
     * @param     mixed $burgerlijkeGescheiden The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function filterByBurgerlijkeGescheiden($burgerlijkeGescheiden = null, $comparison = null)
    {
        if (is_array($burgerlijkeGescheiden)) {
            $useMinMax = false;
            if (isset($burgerlijkeGescheiden['min'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN, $burgerlijkeGescheiden['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($burgerlijkeGescheiden['max'])) {
                $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN, $burgerlijkeGescheiden['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InitialPeopleTableMap::COL_BURGERLIJKE_GESCHEIDEN, $burgerlijkeGescheiden, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildInitialPeople $initialPeople Object to remove from the list of results
     *
     * @return $this|ChildInitialPeopleQuery The current query, for fluid interface
     */
    public function prune($initialPeople = null)
    {
        if ($initialPeople) {
            $this->addUsingAlias(InitialPeopleTableMap::COL_ID, $initialPeople->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the data_initial table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            InitialPeopleTableMap::clearInstancePool();
            InitialPeopleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InitialPeopleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(InitialPeopleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            InitialPeopleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            InitialPeopleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // InitialPeopleQuery

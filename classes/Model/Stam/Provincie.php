<?php

namespace Model\Custom\NovumCbs\Stam;

use Model\Custom\NovumCbs\Stam\Base\Provincie as BaseProvincie;

/**
 * Skeleton subclass for representing a row from the 'provincie' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Provincie extends BaseProvincie
{

}

<?php
namespace Crud\Custom\NovumCbs\Familienaam\Base;

use Crud\Custom\NovumCbs;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCbs\Data\Familienaam;
use Model\Custom\NovumCbs\Data\FamilienaamQuery;
use Model\Custom\NovumCbs\Data\Map\FamilienaamTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Familienaam instead if you need to override or add functionality.
 */
abstract class CrudFamilienaamManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCbs\CrudTrait;
	use NovumCbs\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return FamilienaamQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCbs\Data\Map\FamilienaamTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat alle achternamen die in Nederland voorkomen. .";
	}


	public function getEntityTitle(): string
	{
		return "Familienaam";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Familienamen toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Familienamen aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Prefix', 'Naam', 'InTop', 'Rang', 'Aantal2007', 'Aantal1947', 'CalcAantalHuidigJaar', 'CalcPpm'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Prefix', 'Naam', 'InTop', 'Rang', 'Aantal2007', 'Aantal1947', 'CalcAantalHuidigJaar', 'CalcPpm'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Familienaam
	 */
	public function getModel(array $aData = null): Familienaam
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFamilienaamQuery = FamilienaamQuery::create();
		     $oFamilienaam = $oFamilienaamQuery->findOneById($aData['id']);
		     if (!$oFamilienaam instanceof Familienaam) {
		         throw new LogicException("Familienaam should be an instance of Familienaam but got something else." . __METHOD__);
		     }
		     $oFamilienaam = $this->fillVo($aData, $oFamilienaam);
		} else {
		     $oFamilienaam = new Familienaam();
		     if (!empty($aData)) {
		         $oFamilienaam = $this->fillVo($aData, $oFamilienaam);
		     }
		}
		return $oFamilienaam;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Familienaam
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Familienaam
	{
		$oFamilienaam = $this->getModel($aData);


		 if(!empty($oFamilienaam))
		 {
		     $oFamilienaam = $this->fillVo($aData, $oFamilienaam);
		     $oFamilienaam->save();
		 }
		return $oFamilienaam;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Familienaam $oModel
	 * @return Familienaam
	 */
	protected function fillVo(array $aData, Familienaam $oModel): Familienaam
	{
		isset($aData['prefix']) ? $oModel->setPrefix($aData['prefix']) : null;
		isset($aData['naam']) ? $oModel->setNaam($aData['naam']) : null;
		isset($aData['in_top']) ? $oModel->setInTop($aData['in_top']) : null;
		isset($aData['rang']) ? $oModel->setRang($aData['rang']) : null;
		isset($aData['aantal_2007']) ? $oModel->setAantal2007($aData['aantal_2007']) : null;
		isset($aData['aantal_1947']) ? $oModel->setAantal1947($aData['aantal_1947']) : null;
		isset($aData['calc_aantal_huidig_jaar']) ? $oModel->setCalcAantalHuidigJaar($aData['calc_aantal_huidig_jaar']) : null;
		isset($aData['calc_ppm']) ? $oModel->setCalcPpm($aData['calc_ppm']) : null;
		return $oModel;
	}
}

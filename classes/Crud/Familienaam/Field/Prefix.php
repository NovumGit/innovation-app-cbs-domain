<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field;

use Crud\Custom\NovumCbs\Familienaam\Field\Base\Prefix as BasePrefix;

/**
 * Skeleton subclass for representing prefix field from the data_familienaam table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Prefix extends BasePrefix
{
}

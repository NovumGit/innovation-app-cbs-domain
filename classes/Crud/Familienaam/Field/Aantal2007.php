<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field;

use Crud\Custom\NovumCbs\Familienaam\Field\Base\Aantal2007 as BaseAantal2007;

/**
 * Skeleton subclass for representing aantal_2007 field from the data_familienaam table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Aantal2007 extends BaseAantal2007
{
}

<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field;

use Crud\Custom\NovumCbs\Familienaam\Field\Base\CalcAantalHuidigJaar as BaseCalcAantalHuidigJaar;

/**
 * Skeleton subclass for representing calc_aantal_huidig_jaar field from the data_familienaam table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CalcAantalHuidigJaar extends BaseCalcAantalHuidigJaar
{
}

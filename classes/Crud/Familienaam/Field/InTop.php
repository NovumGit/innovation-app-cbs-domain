<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field;

use Crud\Custom\NovumCbs\Familienaam\Field\Base\InTop as BaseInTop;

/**
 * Skeleton subclass for representing in_top field from the data_familienaam table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class InTop extends BaseInTop
{
}

<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'prefix' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class Prefix extends GenericString implements IFilterableField, IEditableField
{
	protected $sFieldName = 'prefix';

	protected $sFieldLabel = 'Prefix';

	protected $sIcon = 'user';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getPrefix';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

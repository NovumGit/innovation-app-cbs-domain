<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'aantal_1947' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class Aantal1947 extends GenericInteger implements IFilterableField, IEditableField
{
	protected $sFieldName = 'aantal_1947';

	protected $sFieldLabel = 'Aantal 1947';

	protected $sIcon = 'group';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getAantal1947';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'rang' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class Rang extends GenericInteger implements IFilterableField, IEditableField
{
	protected $sFieldName = 'rang';

	protected $sFieldLabel = 'Rang';

	protected $sIcon = 'calendar';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getRang';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

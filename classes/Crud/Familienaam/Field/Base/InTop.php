<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericBoolean;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'in_top' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class InTop extends GenericBoolean implements IFilterableField, IEditableField
{
	protected $sFieldName = 'in_top';

	protected $sFieldLabel = 'In top';

	protected $sIcon = 'user';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getInTop';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

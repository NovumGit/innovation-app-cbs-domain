<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'aantal_2007' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class Aantal2007 extends GenericInteger implements IFilterableField, IEditableField
{
	protected $sFieldName = 'aantal_2007';

	protected $sFieldLabel = 'Aantal 2007';

	protected $sIcon = 'group';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getAantal2007';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'calc_ppm' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class CalcPpm extends GenericInteger implements IFilterableField, IEditableField
{
	protected $sFieldName = 'calc_ppm';

	protected $sFieldLabel = 'Aantal per miljoen';

	protected $sIcon = 'group';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getCalcPpm';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

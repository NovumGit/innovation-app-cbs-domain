<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'calc_aantal_huidig_jaar' crud field from the 'data_familienaam' table.
 * This class is auto generated and should not be modified.
 */
abstract class CalcAantalHuidigJaar extends GenericInteger implements IFilterableField, IEditableField
{
	protected $sFieldName = 'calc_aantal_huidig_jaar';

	protected $sFieldLabel = 'Aantal relatief';

	protected $sIcon = 'group';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getCalcAantalHuidigJaar';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Familienaam';


	public function isUniqueKey(): bool
	{
		return false;
	}
}

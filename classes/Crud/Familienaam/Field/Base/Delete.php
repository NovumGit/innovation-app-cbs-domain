<?php 
namespace Crud\Custom\NovumCbs\Familienaam\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\Data\Familienaam;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Familienaam)
		{
		     return "/custom/novumcbs/datageneratie/data_familienaam/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Familienaam)
		{
		     return "/custom/novumcbs/data_familienaam?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

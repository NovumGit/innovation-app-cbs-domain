<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field;

use Crud\Custom\NovumCbs\Familienaam\Field\Base\Naam as BaseNaam;

/**
 * Skeleton subclass for representing naam field from the data_familienaam table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Naam extends BaseNaam
{
}

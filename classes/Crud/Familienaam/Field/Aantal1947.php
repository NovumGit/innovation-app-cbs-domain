<?php
namespace Crud\Custom\NovumCbs\Familienaam\Field;

use Crud\Custom\NovumCbs\Familienaam\Field\Base\Aantal1947 as BaseAantal1947;

/**
 * Skeleton subclass for representing aantal_1947 field from the data_familienaam table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Aantal1947 extends BaseAantal1947
{
}

<?php
namespace Crud\Custom\NovumCbs\Currenttime\Field;

use Crud\Custom\NovumCbs\Currenttime\Field\Base\HuidigeTijdstip as BaseHuidigeTijdstip;

/**
 * Skeleton subclass for representing naam field from the current_time table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class HuidigeTijdstip extends BaseHuidigeTijdstip
{
}

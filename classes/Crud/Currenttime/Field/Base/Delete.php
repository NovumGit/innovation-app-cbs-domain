<?php 
namespace Crud\Custom\NovumCbs\Currenttime\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\DataGeneratie\Currenttime;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Currenttime)
		{
		     return "/custom/novumcbs/stamtabellen/current_time/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Currenttime)
		{
		     return "/custom/novumcbs/current_time?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

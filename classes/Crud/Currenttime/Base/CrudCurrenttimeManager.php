<?php
namespace Crud\Custom\NovumCbs\Currenttime\Base;

use Crud\Custom\NovumCbs;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCbs\DataGeneratie\Currenttime;
use Model\Custom\NovumCbs\DataGeneratie\CurrenttimeQuery;
use Model\Custom\NovumCbs\DataGeneratie\Map\CurrenttimeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Currenttime instead if you need to override or add functionality.
 */
abstract class CrudCurrenttimeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCbs\CrudTrait;
	use NovumCbs\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return CurrenttimeQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCbs\DataGeneratie\Map\CurrenttimeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint bevat het huidige tijdstip in de geschiedenis of nu en wordt ingezet bij datageneratie.";
	}


	public function getEntityTitle(): string
	{
		return "Currenttime";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumcbs/stamtabellen/current_time/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumcbs/stamtabellen/current_time/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Simuleert de huidige tijd, gebruikt voor datageneratoe toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Simuleert de huidige tijd, gebruikt voor datageneratoe aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['HuidigeTijdstip', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['HuidigeTijdstip'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Currenttime
	 */
	public function getModel(array $aData = null): Currenttime
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCurrenttimeQuery = CurrenttimeQuery::create();
		     $oCurrenttime = $oCurrenttimeQuery->findOneById($aData['id']);
		     if (!$oCurrenttime instanceof Currenttime) {
		         throw new LogicException("Currenttime should be an instance of Currenttime but got something else." . __METHOD__);
		     }
		     $oCurrenttime = $this->fillVo($aData, $oCurrenttime);
		} else {
		     $oCurrenttime = new Currenttime();
		     if (!empty($aData)) {
		         $oCurrenttime = $this->fillVo($aData, $oCurrenttime);
		     }
		}
		return $oCurrenttime;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Currenttime
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Currenttime
	{
		$oCurrenttime = $this->getModel($aData);


		 if(!empty($oCurrenttime))
		 {
		     $oCurrenttime = $this->fillVo($aData, $oCurrenttime);
		     $oCurrenttime->save();
		 }
		return $oCurrenttime;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Currenttime $oModel
	 * @return Currenttime
	 */
	protected function fillVo(array $aData, Currenttime $oModel): Currenttime
	{
		isset($aData['naam']) ? $oModel->setHuidigeTijdstip($aData['naam']) : null;
		return $oModel;
	}
}

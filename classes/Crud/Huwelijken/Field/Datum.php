<?php
namespace Crud\Custom\NovumCbs\Huwelijken\Field;

use Crud\Custom\NovumCbs\Huwelijken\Field\Base\Datum as BaseDatum;

/**
 * Skeleton subclass for representing datum field from the huwelijken table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Datum extends BaseDatum
{
}

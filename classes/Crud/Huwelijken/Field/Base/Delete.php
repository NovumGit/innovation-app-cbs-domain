<?php 
namespace Crud\Custom\NovumCbs\Huwelijken\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\Stam\Huwelijken;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Huwelijken)
		{
		     return "/custom/novumcbs/bevolking/huwelijken/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Huwelijken)
		{
		     return "/custom/novumcbs/huwelijken?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

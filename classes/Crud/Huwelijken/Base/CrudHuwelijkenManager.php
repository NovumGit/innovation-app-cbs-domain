<?php
namespace Crud\Custom\NovumCbs\Huwelijken\Base;

use Crud\Custom\NovumCbs;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCbs\Stam\Huwelijken;
use Model\Custom\NovumCbs\Stam\HuwelijkenQuery;
use Model\Custom\NovumCbs\Stam\Map\HuwelijkenTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Huwelijken instead if you need to override or add functionality.
 */
abstract class CrudHuwelijkenManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCbs\CrudTrait;
	use NovumCbs\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return HuwelijkenQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCbs\Stam\Map\HuwelijkenTableMap();
	}


	public function getShortDescription(): string
	{
		return "Het aantal huwelijken per dag.";
	}


	public function getEntityTitle(): string
	{
		return "Huwelijken";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumcbs/bevolking/huwelijken/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumcbs/bevolking/huwelijken/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Aantal huwelijken per dag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Aantal huwelijken per dag aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Datum', 'Aantal', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Datum', 'Aantal'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Huwelijken
	 */
	public function getModel(array $aData = null): Huwelijken
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oHuwelijkenQuery = HuwelijkenQuery::create();
		     $oHuwelijken = $oHuwelijkenQuery->findOneById($aData['id']);
		     if (!$oHuwelijken instanceof Huwelijken) {
		         throw new LogicException("Huwelijken should be an instance of Huwelijken but got something else." . __METHOD__);
		     }
		     $oHuwelijken = $this->fillVo($aData, $oHuwelijken);
		} else {
		     $oHuwelijken = new Huwelijken();
		     if (!empty($aData)) {
		         $oHuwelijken = $this->fillVo($aData, $oHuwelijken);
		     }
		}
		return $oHuwelijken;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Huwelijken
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Huwelijken
	{
		$oHuwelijken = $this->getModel($aData);


		 if(!empty($oHuwelijken))
		 {
		     $oHuwelijken = $this->fillVo($aData, $oHuwelijken);
		     $oHuwelijken->save();
		 }
		return $oHuwelijken;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Huwelijken $oModel
	 * @return Huwelijken
	 */
	protected function fillVo(array $aData, Huwelijken $oModel): Huwelijken
	{
		isset($aData['datum']) ? $oModel->setDatum($aData['datum']) : null;
		isset($aData['aantal']) ? $oModel->setAantal($aData['aantal']) : null;
		return $oModel;
	}
}

<?php 
namespace Crud\Custom\NovumCbs\Land\Field\Base;

use Crud\Generic\Field\GenericCheckbox;
use Crud\IEventField;
use Crud\IField;

class Checkbox extends GenericCheckbox implements IField, IEventField
{
}

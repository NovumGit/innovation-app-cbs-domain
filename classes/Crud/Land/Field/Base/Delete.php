<?php 
namespace Crud\Custom\NovumCbs\Land\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\Stam\Land;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Land)
		{
		     return "/custom/novumcbs/stamtabellen/land/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Land)
		{
		     return "/custom/novumcbs/land?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

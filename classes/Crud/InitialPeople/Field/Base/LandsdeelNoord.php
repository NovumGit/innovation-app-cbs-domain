<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'landsdeel_noord' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class LandsdeelNoord extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'landsdeel_noord';

	protected $sFieldLabel = 'Personen in noord nederland';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getLandsdeelNoord';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['landsdeel_noord']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Personen in noord nederland" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

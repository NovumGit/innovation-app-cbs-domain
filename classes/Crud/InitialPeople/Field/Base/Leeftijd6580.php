<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'leeftijd_65_80' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class Leeftijd6580 extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'leeftijd_65_80';

	protected $sFieldLabel = 'Personen in de leeftijd 65 tot 80';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getLeeftijd6580';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['leeftijd_65_80']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Personen in de leeftijd 65 tot 80" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

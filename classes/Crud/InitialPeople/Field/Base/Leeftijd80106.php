<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'leeftijd_80_106' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class Leeftijd80106 extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'leeftijd_80_106';

	protected $sFieldLabel = 'Personen in de leeftijd 80 tot 106';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getLeeftijd80106';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['leeftijd_80_106']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Personen in de leeftijd 80 tot 106" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

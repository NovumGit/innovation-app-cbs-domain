<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'leeftijd_0_20' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class Leeftijd020 extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'leeftijd_0_20';

	protected $sFieldLabel = 'Personen in de leeftijd 0 tot 20';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getLeeftijd020';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['leeftijd_0_20']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Personen in de leeftijd 0 tot 20" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

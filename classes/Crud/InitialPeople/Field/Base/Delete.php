<?php 
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\Data\InitialPeople;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof InitialPeople)
		{
		     return "/custom/novumcbs/datageneratie/data_initial/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof InitialPeople)
		{
		     return "/custom/novumcbs/data_initial?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'jaartal' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class Jaartal extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'jaartal';

	protected $sFieldLabel = 'Jaartal';

	protected $sIcon = 'calendar';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getJaartal';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['jaartal']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Jaartal" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'leeftijd_45_65' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class Leeftijd4565 extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'leeftijd_45_65';

	protected $sFieldLabel = 'Personen in de leeftijd 45 tot 65';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getLeeftijd4565';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['leeftijd_45_65']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Personen in de leeftijd 45 tot 65" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

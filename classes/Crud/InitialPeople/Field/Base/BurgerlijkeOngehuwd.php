<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'burgerlijke_ongehuwd' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class BurgerlijkeOngehuwd extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'burgerlijke_ongehuwd';

	protected $sFieldLabel = 'Totaal aantal ongehuwde personen';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getBurgerlijkeOngehuwd';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['burgerlijke_ongehuwd']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Totaal aantal ongehuwde personen" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

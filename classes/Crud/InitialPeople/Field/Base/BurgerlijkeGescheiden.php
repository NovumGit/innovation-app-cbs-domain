<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'burgerlijke_gescheiden' crud field from the 'data_initial' table.
 * This class is auto generated and should not be modified.
 */
abstract class BurgerlijkeGescheiden extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'burgerlijke_gescheiden';

	protected $sFieldLabel = 'Totaal aantal gescheiden personen';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getBurgerlijkeGescheiden';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\InitialPeople';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['burgerlijke_gescheiden']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Totaal aantal gescheiden personen" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field;

use Crud\Custom\NovumCbs\InitialPeople\Field\Base\LandsdeelOost as BaseLandsdeelOost;

/**
 * Skeleton subclass for representing landsdeel_oost field from the data_initial table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class LandsdeelOost extends BaseLandsdeelOost
{
}

<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field;

use Crud\Custom\NovumCbs\InitialPeople\Field\Base\Leeftijd80106 as BaseLeeftijd80106;

/**
 * Skeleton subclass for representing leeftijd_80_106 field from the data_initial table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Leeftijd80106 extends BaseLeeftijd80106
{
}

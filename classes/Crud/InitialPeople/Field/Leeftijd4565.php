<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Field;

use Crud\Custom\NovumCbs\InitialPeople\Field\Base\Leeftijd4565 as BaseLeeftijd4565;

/**
 * Skeleton subclass for representing leeftijd_45_65 field from the data_initial table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Leeftijd4565 extends BaseLeeftijd4565
{
}

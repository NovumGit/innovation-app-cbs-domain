<?php
namespace Crud\Custom\NovumCbs\InitialPeople\Base;

use Crud\Custom\NovumCbs;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCbs\Data\InitialPeople;
use Model\Custom\NovumCbs\Data\InitialPeopleQuery;
use Model\Custom\NovumCbs\Data\Map\InitialPeopleTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify InitialPeople instead if you need to override or add functionality.
 */
abstract class CrudInitialPeopleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCbs\CrudTrait;
	use NovumCbs\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return InitialPeopleQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCbs\Data\Map\InitialPeopleTableMap();
	}


	public function getShortDescription(): string
	{
		return "In deze tabel staat de te genereren data, dus welke mensen waren er al voor het begin der tijden vanuit database perspectief. .";
	}


	public function getEntityTitle(): string
	{
		return "InitialPeople";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Begin der tijden toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Begin der tijden aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Jaartal', 'Mannen', 'Vrouwen', 'Leeftijd020', 'Leeftijd2045', 'Leeftijd4565', 'Leeftijd6580', 'Leeftijd80106', 'LandsdeelNoord', 'LandsdeelOost', 'LandsdeelWest', 'LandsdeelZuid', 'BurgerlijkeOngehuwd', 'BurgerlijkeGehuwd', 'BurgerlijkeGescheiden'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Jaartal', 'Mannen', 'Vrouwen', 'Leeftijd020', 'Leeftijd2045', 'Leeftijd4565', 'Leeftijd6580', 'Leeftijd80106', 'LandsdeelNoord', 'LandsdeelOost', 'LandsdeelWest', 'LandsdeelZuid', 'BurgerlijkeOngehuwd', 'BurgerlijkeGehuwd', 'BurgerlijkeGescheiden'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return InitialPeople
	 */
	public function getModel(array $aData = null): InitialPeople
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oInitialPeopleQuery = InitialPeopleQuery::create();
		     $oInitialPeople = $oInitialPeopleQuery->findOneById($aData['id']);
		     if (!$oInitialPeople instanceof InitialPeople) {
		         throw new LogicException("InitialPeople should be an instance of InitialPeople but got something else." . __METHOD__);
		     }
		     $oInitialPeople = $this->fillVo($aData, $oInitialPeople);
		} else {
		     $oInitialPeople = new InitialPeople();
		     if (!empty($aData)) {
		         $oInitialPeople = $this->fillVo($aData, $oInitialPeople);
		     }
		}
		return $oInitialPeople;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return InitialPeople
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): InitialPeople
	{
		$oInitialPeople = $this->getModel($aData);


		 if(!empty($oInitialPeople))
		 {
		     $oInitialPeople = $this->fillVo($aData, $oInitialPeople);
		     $oInitialPeople->save();
		 }
		return $oInitialPeople;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param InitialPeople $oModel
	 * @return InitialPeople
	 */
	protected function fillVo(array $aData, InitialPeople $oModel): InitialPeople
	{
		isset($aData['jaartal']) ? $oModel->setJaartal($aData['jaartal']) : null;
		isset($aData['mannen']) ? $oModel->setMannen($aData['mannen']) : null;
		isset($aData['vrouwen']) ? $oModel->setVrouwen($aData['vrouwen']) : null;
		isset($aData['leeftijd_0_20']) ? $oModel->setLeeftijd020($aData['leeftijd_0_20']) : null;
		isset($aData['leeftijd_20_45']) ? $oModel->setLeeftijd2045($aData['leeftijd_20_45']) : null;
		isset($aData['leeftijd_45_65']) ? $oModel->setLeeftijd4565($aData['leeftijd_45_65']) : null;
		isset($aData['leeftijd_65_80']) ? $oModel->setLeeftijd6580($aData['leeftijd_65_80']) : null;
		isset($aData['leeftijd_80_106']) ? $oModel->setLeeftijd80106($aData['leeftijd_80_106']) : null;
		isset($aData['landsdeel_noord']) ? $oModel->setLandsdeelNoord($aData['landsdeel_noord']) : null;
		isset($aData['landsdeel_oost']) ? $oModel->setLandsdeelOost($aData['landsdeel_oost']) : null;
		isset($aData['landsdeel_west']) ? $oModel->setLandsdeelWest($aData['landsdeel_west']) : null;
		isset($aData['landsdeel_zuid']) ? $oModel->setLandsdeelZuid($aData['landsdeel_zuid']) : null;
		isset($aData['burgerlijke_ongehuwd']) ? $oModel->setBurgerlijkeOngehuwd($aData['burgerlijke_ongehuwd']) : null;
		isset($aData['burgerlijke_gehuwd']) ? $oModel->setBurgerlijkeGehuwd($aData['burgerlijke_gehuwd']) : null;
		isset($aData['burgerlijke_gescheiden']) ? $oModel->setBurgerlijkeGescheiden($aData['burgerlijke_gescheiden']) : null;
		return $oModel;
	}
}

<?php
namespace Crud\Custom\NovumCbs\Geboorte\Base;

use Crud\Custom\NovumCbs;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCbs\Data\Geboorte;
use Model\Custom\NovumCbs\Data\GeboorteQuery;
use Model\Custom\NovumCbs\Data\Map\GeboorteTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Geboorte instead if you need to override or add functionality.
 */
abstract class CrudGeboorteManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCbs\CrudTrait;
	use NovumCbs\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return GeboorteQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCbs\Data\Map\GeboorteTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint vind je hoeveel jongetjes en hoeveel meisjes er geboren zijn per dag sinds het jaar 1900. Dit endpoint is bedold voor data generatie.";
	}


	public function getEntityTitle(): string
	{
		return "Geboorte";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Geboorte cijfers toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Geboorte cijfers aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Datum', 'JongensGeboren', 'MeisjesGeboren'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Datum', 'JongensGeboren', 'MeisjesGeboren'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Geboorte
	 */
	public function getModel(array $aData = null): Geboorte
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oGeboorteQuery = GeboorteQuery::create();
		     $oGeboorte = $oGeboorteQuery->findOneById($aData['id']);
		     if (!$oGeboorte instanceof Geboorte) {
		         throw new LogicException("Geboorte should be an instance of Geboorte but got something else." . __METHOD__);
		     }
		     $oGeboorte = $this->fillVo($aData, $oGeboorte);
		} else {
		     $oGeboorte = new Geboorte();
		     if (!empty($aData)) {
		         $oGeboorte = $this->fillVo($aData, $oGeboorte);
		     }
		}
		return $oGeboorte;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Geboorte
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Geboorte
	{
		$oGeboorte = $this->getModel($aData);


		 if(!empty($oGeboorte))
		 {
		     $oGeboorte = $this->fillVo($aData, $oGeboorte);
		     $oGeboorte->save();
		 }
		return $oGeboorte;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Geboorte $oModel
	 * @return Geboorte
	 */
	protected function fillVo(array $aData, Geboorte $oModel): Geboorte
	{
		isset($aData['datum']) ? $oModel->setDatum($aData['datum']) : null;
		isset($aData['jongens_geboren']) ? $oModel->setJongensGeboren($aData['jongens_geboren']) : null;
		isset($aData['meisjes_geboren']) ? $oModel->setMeisjesGeboren($aData['meisjes_geboren']) : null;
		return $oModel;
	}
}

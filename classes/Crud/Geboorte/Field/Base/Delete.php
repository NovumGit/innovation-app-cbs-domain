<?php 
namespace Crud\Custom\NovumCbs\Geboorte\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\Data\Geboorte;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Geboorte)
		{
		     return "/custom/novumcbs/datageneratie/data_geboorte/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Geboorte)
		{
		     return "/custom/novumcbs/data_geboorte?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

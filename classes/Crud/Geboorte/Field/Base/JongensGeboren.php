<?php
namespace Crud\Custom\NovumCbs\Geboorte\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'jongens_geboren' crud field from the 'data_geboorte' table.
 * This class is auto generated and should not be modified.
 */
abstract class JongensGeboren extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'jongens_geboren';

	protected $sFieldLabel = 'Aantal jongens geboren';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getJongensGeboren';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Geboorte';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['jongens_geboren']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Aantal jongens geboren" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

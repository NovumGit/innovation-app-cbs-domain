<?php
namespace Crud\Custom\NovumCbs\Geboorte\Field\Base;

use Crud\Generic\Field\GenericDate;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'datum' crud field from the 'data_geboorte' table.
 * This class is auto generated and should not be modified.
 */
abstract class Datum extends GenericDate implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'datum';

	protected $sFieldLabel = 'Datum';

	protected $sIcon = 'calendar';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getDatum';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Geboorte';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['datum']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Datum" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

<?php
namespace Crud\Custom\NovumCbs\Geboorte\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'meisjes_geboren' crud field from the 'data_geboorte' table.
 * This class is auto generated and should not be modified.
 */
abstract class MeisjesGeboren extends GenericInteger implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'meisjes_geboren';

	protected $sFieldLabel = 'Aantal meisjes geboren';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getMeisjesGeboren';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\Data\Geboorte';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['meisjes_geboren']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Aantal meisjes geboren" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

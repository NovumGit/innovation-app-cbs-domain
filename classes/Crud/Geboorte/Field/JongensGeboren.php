<?php
namespace Crud\Custom\NovumCbs\Geboorte\Field;

use Crud\Custom\NovumCbs\Geboorte\Field\Base\JongensGeboren as BaseJongensGeboren;

/**
 * Skeleton subclass for representing jongens_geboren field from the data_geboorte table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class JongensGeboren extends BaseJongensGeboren
{
}

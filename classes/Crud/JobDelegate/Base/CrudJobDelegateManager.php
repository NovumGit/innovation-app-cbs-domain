<?php
namespace Crud\Custom\NovumCbs\JobDelegate\Base;

use Crud\Custom\NovumCbs;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumCbs\DataGeneratie\JobDelegate;
use Model\Custom\NovumCbs\DataGeneratie\JobDelegateQuery;
use Model\Custom\NovumCbs\DataGeneratie\Map\JobDelegateTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify JobDelegate instead if you need to override or add functionality.
 */
abstract class CrudJobDelegateManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumCbs\CrudTrait;
	use NovumCbs\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return JobDelegateQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumCbs\DataGeneratie\Map\JobDelegateTableMap();
	}


	public function getShortDescription(): string
	{
		return "Taken die vanuit dit endpoint verzonden zijn";
	}


	public function getEntityTitle(): string
	{
		return "JobDelegate";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Instructies verzonden toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Instructies verzonden aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Topic', 'Payload'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Topic', 'Payload'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return JobDelegate
	 */
	public function getModel(array $aData = null): JobDelegate
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oJobDelegateQuery = JobDelegateQuery::create();
		     $oJobDelegate = $oJobDelegateQuery->findOneById($aData['id']);
		     if (!$oJobDelegate instanceof JobDelegate) {
		         throw new LogicException("JobDelegate should be an instance of JobDelegate but got something else." . __METHOD__);
		     }
		     $oJobDelegate = $this->fillVo($aData, $oJobDelegate);
		} else {
		     $oJobDelegate = new JobDelegate();
		     if (!empty($aData)) {
		         $oJobDelegate = $this->fillVo($aData, $oJobDelegate);
		     }
		}
		return $oJobDelegate;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return JobDelegate
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): JobDelegate
	{
		$oJobDelegate = $this->getModel($aData);


		 if(!empty($oJobDelegate))
		 {
		     $oJobDelegate = $this->fillVo($aData, $oJobDelegate);
		     $oJobDelegate->save();
		 }
		return $oJobDelegate;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param JobDelegate $oModel
	 * @return JobDelegate
	 */
	protected function fillVo(array $aData, JobDelegate $oModel): JobDelegate
	{
		isset($aData['topic']) ? $oModel->setTopic($aData['topic']) : null;
		isset($aData['payload']) ? $oModel->setPayload($aData['payload']) : null;
		return $oModel;
	}
}

<?php
namespace Crud\Custom\NovumCbs\JobDelegate\Field\Base;

use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'payload' crud field from the 'job_delegate' table.
 * This class is auto generated and should not be modified.
 */
abstract class Payload extends GenericTextarea implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'payload';

	protected $sFieldLabel = 'Bericht';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getPayload';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\DataGeneratie\JobDelegate';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['payload']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Bericht" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

<?php
namespace Crud\Custom\NovumCbs\JobDelegate\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'topic' crud field from the 'job_delegate' table.
 * This class is auto generated and should not be modified.
 */
abstract class Topic extends GenericString implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'topic';

	protected $sFieldLabel = 'Onderwerp';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getTopic';

	protected $sFqModelClassname = '\Model\Custom\NovumCbs\DataGeneratie\JobDelegate';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['topic']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Onderwerp" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}

<?php
namespace Crud\Custom\NovumCbs\JobDelegate\Field;

use Crud\Custom\NovumCbs\JobDelegate\Field\Base\Payload as BasePayload;

/**
 * Skeleton subclass for representing payload field from the job_delegate table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Payload extends BasePayload
{
}

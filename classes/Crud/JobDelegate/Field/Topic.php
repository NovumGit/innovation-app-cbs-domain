<?php
namespace Crud\Custom\NovumCbs\JobDelegate\Field;

use Crud\Custom\NovumCbs\JobDelegate\Field\Base\Topic as BaseTopic;

/**
 * Skeleton subclass for representing topic field from the job_delegate table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Topic extends BaseTopic
{
}

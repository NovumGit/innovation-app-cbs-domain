<?php 
namespace Crud\Custom\NovumCbs\Geslacht\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumCbs\Stam\Geslacht;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Geslacht)
		{
		     return "/custom/novumcbs/stamtabellen/geslacht/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Geslacht)
		{
		     return "/custom/novumcbs/geslacht?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}

<?php

/***
* This file is generated, please do not modify manually.
*/

if(isset($_SERVER['IS_DEVEL']))
{
    $aConfig = [
        'PROTOCOL' => 'http',
        'ADMIN_PROTOCOL' => 'http',
        'CUSTOM_FOLDER' => 'NovumCbs',
        'ABSOLUTE_ROOT' => $_SERVER['SYSTEM_ROOT'],
        'DOMAIN' => 'cbs.demo.novum.nuidev.nl',
        /* Je zoekt waarschijnlijk Config::getDataDir() */
        'DATA_DIR' => '../'
    ];
}
else
{
    $aConfig = [
        'PROTOCOL' => 'https',
        'ADMIN_PROTOCOL' => 'https',
        'CUSTOM_FOLDER' => 'NovumCbs',
        'DOMAIN' => 'cbs.demo.novum.nu',
        'ABSOLUTE_ROOT' => '/var/www/1overheid/cbs/system/',
        'DATA_DIR' => '/home/novum/data/cbs'
    ];
}

$aConfig['CUSTOM_NAMESPACE'] = 'NovumCbs';

return $aConfig;

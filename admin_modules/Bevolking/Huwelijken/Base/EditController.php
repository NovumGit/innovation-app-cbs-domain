<?php
namespace AdminModules\Custom\NovumCbs\Bevolking\Huwelijken\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumCbs\Huwelijken\CrudHuwelijkenManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumCbs\Bevolking\Huwelijken instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudHuwelijkenManager();
	}


	public function getPageTitle(): string
	{
		return "Aantal huwelijken per dag";
	}
}

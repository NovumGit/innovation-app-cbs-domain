<?php
namespace AdminModules\Custom\NovumCbs\Bevolking\Huwelijken\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Custom\NovumCbs\Huwelijken\CrudHuwelijkenManager;
use Crud\FormManager;
use Model\Custom\NovumCbs\Stam\Huwelijken;
use Model\Custom\NovumCbs\Stam\HuwelijkenQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumCbs\Bevolking\Huwelijken instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Aantal huwelijken per dag";
	}


	public function getModule(): string
	{
		return "Huwelijken";
	}


	public function getManager(): FormManager
	{
		return new CrudHuwelijkenManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return HuwelijkenQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Huwelijken){
		    LogActivity::register("Bevolking", "Aantal huwelijken per dag verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Aantal huwelijken per dag verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Aantal huwelijken per dag niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Aantal huwelijken per dag item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
